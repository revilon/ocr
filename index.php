<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 16.11.2016
 * Time: 1:37
 */
require __DIR__ . '/vendor/autoload.php';
?>
<html>
<head>
</head>
<body>
<h2><p><b> Форма для загрузки картнки </b></p></h2>
<form action="/index.php" method="post" enctype="multipart/form-data">
    <input type="file" name="filename"><br>
    <select name="lang">
        <option value="rus">Русский</option>
        <option value="eng">Английский</option>
    </select>
    <input type="submit" value="Go ocr">
</form>

<?php
if ($_FILES["filename"]["size"] > 1024 * 3 * 1024) {
    echo("Размер файла превышает три мегабайта");
    exit;
}
// Проверяем загружен ли файл
if (is_uploaded_file($_FILES["filename"]["tmp_name"])) {
    // Если файл загружен успешно, перемещаем его
    // из временной директории в конечную
    $resultUpload = move_uploaded_file($_FILES["filename"]["tmp_name"], "/var/www/ocr/images/" . $_FILES["filename"]["name"]);

    if ($resultUpload === true) {
        echo "<hr><p>";
        echo (new TesseractOCR('images/' . $_FILES["filename"]["name"]))
            ->lang($_POST['lang'])
            ->run();
        echo "</p>";
    }
}
?>

</body>
</html>
