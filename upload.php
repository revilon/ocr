<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 16.11.2016
 * Time: 1:47
 */
?>
<html>
<head>
  <title>Результат загрузки файла</title>
</head>
<body>
<?php
   if($_FILES["filename"]["size"] > 1024*3*1024)
   {
       echo ("Размер файла превышает три мегабайта");
       exit;
   }
   // Проверяем загружен ли файл
   if(is_uploaded_file($_FILES["filename"]["tmp_name"]))
   {
       // Если файл загружен успешно, перемещаем его
       // из временной директории в конечную
       move_uploaded_file($_FILES["filename"]["tmp_name"], "/var/www/ocr/images/".$_FILES["filename"]["name"]);
       echo ("Картинка [{$_FILES["filename"]["name"]}] успешно загружена<br>");
       echo "Используйте ее имя в форме распознования текста.<br><a href='/'>Вернуться на главную</a>";
   } else {
       echo("Ошибка загрузки файла");
   }
?>
</body>
</html>